#include "bras.h"

void nomPosition(char *nom, PositionBrasI positionBrasI) {
    char nomsPosition[][16] = {"TRANSPORT", "GALERIE_HAUT", "GALERIE_BAS", "SOL", "COTE_INCLINE", "COTE_HORIZONTAL", "REPLIE", "DEPLOIE"};
    strcpy(nom, nomsPosition[(int) positionBrasI]);
}

typedef struct {
    Angle angle1deg;
    Angle angle2deg;
} PositionBras;

PositionBras positionsBras[] = {
    {140,   39},    // Transport
    {140,   39},    // Gallerie haut
    {119,   20},    // Gallerie bas
    { 26,   16},    // Sol
    { 32,  -33},    // Coté incliné
    { 73,   74},    // Coté horizontal
    {  4,  -90},    // Plié
    { 98,    0}     // Déployé
};
Angle angle1min = -3, angle2min = -108;

void BrasVentouse::init(const int pinServo1, const int pinServo2, const Bornier bornier) {
    currentPosI = TRANSPORT;

    servo1 = new Servo_();
    servo1->init(pinServo1, positionsBras[currentPosI].angle1deg, angle1min, 22, 170);
    servo2 = new Servo_();
    // delay(1000);
    servo2->init(pinServo2, positionsBras[currentPosI].angle2deg, angle2min, 22, 170);

    pump = new Pump();
    pump->init(bornier);
}

void BrasVentouse::move(PositionBrasI positionBrasI) {
    char nom[20];
    nomPosition(nom, currentPosI);
    Serial.print(nom);
    Serial.print(" -> ");
    nomPosition(nom, positionBrasI);
    Serial.println(nom);

    int delay1;
    int delay2;

    switch (positionBrasI) {
        case SOL:
            //delay1 = servo1->setAngle((Angle) 55);
            // delay2 = servo2->setAngle((Angle) 39);
            delay2 = servo2->setAngle(angle2min + 145);
            delay1 = servo1->setAngle(angle2min + 145 + 30);
            delay(delay1 > delay2 ? delay1 : delay2);

            //delay(5000);

            servo1->setAngle(positionsBras[positionBrasI].angle1deg);
            servo2->setAngle(positionsBras[positionBrasI].angle2deg);
            break;
        
        default:
            servo1->setAngle(positionsBras[positionBrasI].angle1deg);
            servo2->setAngle(positionsBras[positionBrasI].angle2deg);
            break;
    }

    currentPosI = positionBrasI;

    /*switch (positionBrasI) {
        case TRANSPORT:
            Serial.println("TRANSPORT");
            servo1->write(positionsBras[0].angle1deg);
            servo2->write(positionsBras[0].angle2deg);
            currentPosI = positionBrasI;
            break;

        case GALERIE_HAUT:
            Serial.println("GALERIE_HAUT");
            servo1->write(positionsBras[1].angle1deg);
            servo2->write(positionsBras[1].angle2deg);
            currentPosI = positionBrasI;
            break;

        default:
            Serial.print("Erreur : Position de bras inconnue : ");
            Serial.println(positionBrasI);
            break;
    }*/
}

void BrasVentouse::setAspiration(const bool onOff) {
    pump->onOff(onOff);
}

PositionBrasI BrasVentouse::getPosition() {
    return currentPosI;
}

void BrasVentouse::debug_move(int servo, int angle) {
    if (servo == 1) {
        servo1->setAngle(angle);
    } else if (servo == 2) {
        servo2->setAngle(angle);
    }
}
