#ifndef BRAS
#define BRAS

#include "pump.h"
#include "servo_.h"

typedef enum {TRANSPORT, GALERIE_HAUT, GALERIE_BAS, SOL, COTE_INCLINE, COTE_HORIZONTAL, REPLIE, DEPLOIE} PositionBrasI;

void nomPosition(char *nom, PositionBrasI positionBrasI);

class BrasVentouse {
    private:
        Servo_* servo1;
        Servo_* servo2;
        Pump* pump;

        PositionBrasI currentPosI;
    public:
        void init(const int pinServo1, const int pinServo2, const Bornier bornier);

        void move(PositionBrasI positionBrasI);
        void setAspiration(const bool);
        PositionBrasI getPosition();

        void debug_move(int servo, int angle);
};

#endif