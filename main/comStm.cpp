#include "comStm.h"
#include <Arduino.h>

void ComStm::init(const int pinBras1, const int pinBras2, const int pinBras3, const int pinPump) {
    this->pinBras1 = pinBras1;
    this->pinBras2 = pinBras2;
    this->pinBras3 = pinBras3;
    this->pinPump = pinPump;

    pinMode(pinBras1, INPUT);
    pinMode(pinBras2, INPUT);
    pinMode(pinBras3, INPUT);
    pinMode(pinPump, INPUT);
}

PositionBrasI ComStm::readPosition() {
    int fil1 = digitalRead(pinBras1);
    int fil2 = digitalRead(pinBras2);
    int fil3 = digitalRead(pinBras3);
    PositionBrasI pos = (PositionBrasI) ((fil3<<2) + (fil2<<1) + fil1);
    // PositionBrasI positionBrasI = (PositionBrasI) ((digitalRead(pinBras3) << 2) + (digitalRead(pinBras2) << 1) + (digitalRead(pinBras1) << 0));

    /*Serial.print("\tread position : 0b");
    Serial.print(fil3);
    Serial.print(fil2);
    Serial.print(fil1);
    Serial.print(" = ");
    Serial.print(pos);*/

    return pos;
}
int ComStm::positionUpdate() {
    PositionBrasI pos = readPosition();
    if (position != pos) {
        delay(10);
        position = readPosition();
        return 1;
    } else
        return 0;
}
PositionBrasI ComStm::getPosition() {
    return position;
}
PositionBrasI ComStm::getLastPosition() {
    return position;
}

int ComStm::readPump() {
    int pump = digitalRead(pinPump);
    /*Serial.print("\tread pump = ");
    Serial.print(pump);*/
    return pump;
}
int ComStm::pumpUpdate() {
    int currentPump = readPump();
    if (pump != currentPump) {
        delay(10);
        pump = readPump();
        return 1;
    } else
        return 0;
}
int ComStm::getPump() {
    return pump;
}
