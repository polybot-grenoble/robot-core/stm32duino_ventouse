#ifndef COM_STM
#define COM_STM

#include "bras.h"

class ComStm {
    private:
        int pinBras1;
        int pinBras2;
        int pinBras3;
        int pinPump;
        PositionBrasI position;
        int pump;
        PositionBrasI readPosition();
        int readPump();
    public:
        void init(const int pinBras1, const int pinBras2, const int pinBras3, const int pinPump);

        int positionUpdate();
        PositionBrasI getPosition();
        PositionBrasI getLastPosition();

        int pumpUpdate();
        int getPump();
};



#endif