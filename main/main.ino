#include "bras.h"
#include "ComStm.h"

#define pinServo1 10
#define pinServo2 11
#define bornierPompe J1

#define filPos1 A2
#define filPos2 A3
#define filPos3 A4
#define filPump A5

BrasVentouse* brasVentouse;
ComStm* comStm;

void serialFlush() {
    delay(50);
    while (Serial.available() > 0) {
        Serial.read();
        delay(50);
    }
    // Serial.println("Serial input flushed");
}

void setup() {
    Serial.begin(115200);
    Serial.println("/*Starting");
    brasVentouse = new BrasVentouse();
    brasVentouse->init(pinServo1, pinServo2, bornierPompe);
    comStm = new ComStm();
    comStm->init(filPos1, filPos2, filPos3, filPump);

    brasVentouse->move(TRANSPORT);
    Serial.println("Started*/");
}

int slaveMode = 0;

void loop() {
    if (Serial.available()) {
        char c = Serial.read();
        if ('0' <= c && c <= '9') {
            int positionBrasI = c - '0';
            brasVentouse->move((PositionBrasI) positionBrasI);
        } else if (c == 'o' || c == 'O' || c == 'y' || c == 'Y') {
            brasVentouse->setAspiration(true);
        } else if (c == 'n' || c == 'N') {
            brasVentouse->setAspiration(false);
        } else if (c == 'S') {
            while (!Serial.available());
            int servo = Serial.read() - '0';
            int angle = Serial.parseInt();
            Serial.print("Raw servo ");
            Serial.print(servo);
            Serial.print(" -> ");
            Serial.println(angle);
            brasVentouse->debug_move(servo, angle);
        } else if (c == 's') {
            slaveMode = !slaveMode;
        }
        serialFlush();
    }

    if (comStm->positionUpdate()) {
        Serial.print("Demande de position : ");
        Serial.print(comStm->getPosition());
        Serial.print(", bits : ");
        Serial.print(comStm->getPosition()>>2&1);
        Serial.print(comStm->getPosition()>>1&1);
        Serial.println(comStm->getPosition()&1);
        // Serial.print("Position updated : ");
        // int pos = (int) comStm->getPosition();
        // Serial.println(pos);
        if (slaveMode)
            brasVentouse->move(comStm->getPosition());
    }
    if (comStm->pumpUpdate()) {
        Serial.print("Demande d'aspiration : ");
        Serial.println(comStm->getPump());
        Serial.print("Pump updated : ");
        if (slaveMode)
            brasVentouse->setAspiration(comStm->getPump());
    }
    // Serial.println();
    // delay(100);
}
