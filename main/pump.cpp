#include "pump.h"

void Pump::init(const Bornier bornier) {
    pumpPin = pinOfBornier(bornier);
    pinMode(pumpPin, OUTPUT);
    currentlyOn = false;
}

void Pump::onOff(const bool mode) {
    digitalWrite(pumpPin, mode);
    currentlyOn = mode;
    Serial.println(mode ? "Pompe allumée" : "Pompe éteinte");
    Serial.println(pumpPin);
}

void Pump::test() {
    while (1) {
        if (Serial.available()) {
            char key = Serial.read();
            if (key == 'q')
                break;
            else {
                while (Serial.available())
                    Serial.read();
            }
        }

        onOff(true);
        delay(500);
        onOff(false);
        delay(500);
    }
}

bool Pump::isOn() {
    return currentlyOn;
}
