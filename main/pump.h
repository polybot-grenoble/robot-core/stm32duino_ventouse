#ifndef PUMP
#define PUMP

#include <Arduino.h>
#include "shieldRelais.h"

class Pump {
    private:
        int pumpPin;
        bool currentlyOn;
    public:
        void init(const Bornier bornier);

        void test();
        void onOff(const bool);
        bool isOn();
};

#endif
