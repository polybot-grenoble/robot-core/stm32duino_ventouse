#include "servo_.h"

void Servo_::init(const int pin, const Angle angleDeg, const Angle minAngle, const int minPos,  const int maxPos) {
    servo.attach(pin);
    setAngle(angleDeg);
    angleMin = minAngle;
    this->minPos = minPos; // TODO not working :'(
    this->maxPos = maxPos; // TODO not working :'(
    /*Serial.print("init maxPos = ");
    Serial.println(this->maxPos);*/
}

/* return ms to move */
int Servo_::setAngle(const Angle angleDeg) {
    int pos = angleDeg - angleMin + 22; // minPos
    // Serial.println(minPos);
    if (pos > 170) { // maxPos not working :'(
        Serial.print("Servo exceeded max pos (");
        Serial.print(pos);
        Serial.print(" > ");
        Serial.print(170);
        Serial.println(")");
        return 0;
    } else {
        servo.write(pos);
        Serial.print("angle = ");
        Serial.print(angleDeg);
        Serial.print(", pos = ");
        Serial.print(pos);
        int travelTimeMs = (angleDeg < angle ? angle - angleDeg : angleDeg - angle) * 600 / 148;
        Serial.print(" -> take ");
        Serial.print(travelTimeMs);
        Serial.println(" ms");
        angle = angleDeg;
        return travelTimeMs;
    }
}

Angle Servo_::getAngle() {
    return angle;
}

void Servo_::test() {
    int mini = 80, maxi = 100, del = 500, debuging = 1;

    while(debuging) {
        servo.write(mini);
        delay(del);
        servo.write(maxi);
        delay(del);

        if (Serial.available()) {
            char key = Serial.read();
            switch (key) {
                case 'd':
                    del = Serial.parseInt();
                    Serial.read();
                    Serial.print("delay = ");
                    Serial.println(del);
                    break;
                case 'h':
                    maxi = Serial.parseInt();
                    Serial.read();
                    Serial.print("max = ");
                    Serial.println(maxi);
                    break;
                case 'l':
                    mini = Serial.parseInt();
                    Serial.read();
                    Serial.print("min = ");
                    Serial.println(mini);
                    break;
                case 'q':
                    while (Serial.available())
                            Serial.read();
                    debuging = 0;
                    break;
            
                default:
                    Serial.println("Syntax error, emptying buffer...");
                    while (Serial.available())
                        Serial.read();
                    break;
            }
        }
    }
}
