#ifndef SERVO_
#define SERVO_

/* total 145° (de 22 à 170) en ~600ms
 * soit ~241°/s
 */

#include <Arduino.h>
#include <Servo.h>

typedef int Angle; // angle en dergé


class Servo_ {
    private:
        Servo servo;
        Angle angle;
        Angle angleMin;
        int minPos;
        int maxPos;
    public:
        void init(const int pin, const Angle angleDeg, const Angle minAngle, const int minPos,  const int maxPos);

        /* return ms to move */
        int setAngle(const Angle angleDeg);
        Angle getAngle();
        void test();
};

#endif
