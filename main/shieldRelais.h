#ifndef SHIELD_RELAIS
#define SHIELD_RELAIS

typedef enum Bornier {J1, J2, J3, J4} Bornier;

int pinOfBornier(const Bornier bornier);

#endif